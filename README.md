# SolidState

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://solidstateapps.gitlab.io/SolidState/)
[![Build Status](https://gitlab.com/solidstateapps/SolidState/badges/master/pipeline.svg)](https://gitlab.com/solidstateapps/SolidState/pipelines)
[![Coverage](https://gitlab.com/solidstateapps/SolidState/badges/master/coverage.svg)](https://gitlab.com/solidstateapps/SolidState/commits/master)
